const express = require('express');
const ErrorHandler = require('./middleware/error.js');
const app = express();
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const path = require('path');
const cors = require('cors');

app.use(express.json());
app.use(cookieParser());
app.use(
  cors({
    origin: 'http://localhost:3000',
    credentials: true,
  })
);

app.use('/', express.static(path.join(__dirname, '/uploads')));
app.use(
  bodyParser.urlencoded({
    extended: true,
    limit: '50mb',
  })
);

// config
if (process.env.NODE_ENV !== 'production') {
  require('dotenv').config({
    path: './config/.env',
  });
}

// import routes
const user = require('./controller/user');
const shop = require('./controller/shop');
const product = require('./controller/product');
const event = require('./controller/event');
const coupon = require('./controller/couponCode');

app.use('/api/v2/user', user);
app.use('/api/v2/shop', shop);
app.use('/api/v2/product', product);
app.use('/api/v2/event', event);
app.use('/api/v2/coupon', coupon);

// It's for Error handling
app.use(ErrorHandler);
module.exports = app;
