const app = require('./app.js');
const connectDatabase = require('./db/Database.js');
// Handling uncaught Exceptions
process.on('uncaughtException', (err) => {
  console.log(`Error: ${err.message}`);
  console.log(`Shutting down the server for handling uncaught exceptions`);
});

// config
if (process.env.NODE_ENV !== 'production') {
  require('dotenv').config({
    path: './config/.env',
  });
}

// Connect db
connectDatabase();

// Create Server
const server = app.listen(process.env.PORT, () => {
  console.log(`Server is running on PORT: ${process.env.PORT}`);
});

// unhandled promise rejection
process.on('unhandledRejection', (err) => {
  console.log(`Shutting down the server for ${err.message}`);
  console.log(`Shutting down the server for unhandled promise rejection`);

  server.close(() => {
    process.exit(1);
  });
});
