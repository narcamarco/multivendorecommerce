import React, { useEffect, useState, useRef } from 'react';
import { useParams } from 'react-router-dom';
import { server } from '../server';
import axios from 'axios';

const ActivationPage = () => {
  const { activation_token } = useParams();
  const [error, setError] = useState(false);
  const isRender = useRef(false);

  useEffect(() => {
    if (isRender.current) {
      if (activation_token) {
        const activationEmail = async () => {
          try {
            const res = await axios.post(`${server}/user/activation`, {
              activation_token,
            });

            console.log(res.data.message);
          } catch (err) {
            console.log(err.response.data.message);
            setError(true);
          }
        };

        activationEmail();
      }
    }

    return () => {
      isRender.current = true;
    };
  }, []);

  return (
    <div className="w-full h-screen flex justify-center items-center">
      {error ? (
        <p>Your token is expired</p>
      ) : (
        <p>Your account has been created successfully</p>
      )}
    </div>
  );
};

export default ActivationPage;
